const debug = require('debug')
const fs = require('fs').promises
const fse = require('fs-extra')
const ical = require('ical-generator')
const yaml = require('js-yaml')
const _ = require('lodash')
const moment = require('moment-timezone')
const path = require('path')
const process = require('process')
const walk = require('walkdir')

const { MarkdownFile } = require('../lib/markdown')


const projPath = path.join(__dirname, '..', '..')
const contentPath = path.join(projPath, 'build');
const eventsPath = path.join(contentPath, 'events')
const indexName = 'index.md'
const httpScheme = 'http'

const xKeyFilter = key => key.toLowerCase().startsWith('x-')


const eventLog = debug('vicpah.gitlab.io:event')
const calendarLog = debug('vicpah.gitlab.io:calendar')

const vicpahContact = { name: 'VicPAH', email: 'bark@vicpah.org.au' }


class Event {
  constructor({ thisPath }) {
    eventLog(`New: ${thisPath}`)
    this.thisPath = thisPath
    this.mdFile = new MarkdownFile(thisPath)
    this.getOrganizer = this.getOrganizer.bind(this)
    this.getLocation = this.getLocation.bind(this)
    this.getSlug = this.getSlug.bind(this)
    this.getFrontmatter = this.mdFile.getFrontmatter
    this.getContentText = this.mdFile.getContentText
    this.getContentHtml = this.mdFile.getContentHtml
  }
  async getOrganizer() {
    const { organizer, eventGroups = [] } = await this.getFrontmatter()
    if (organizer) {
      return organizer;
    }
    if (eventGroups.indexOf('vicpah') >= 0) {
      return vicpahContact;
    }
    return null;
  }
  async getLocation() {
    const { adr } = await this.getFrontmatter()
    if (!adr) {
      return null
    }
    const lines = [
      adr.street_address,
      adr.suburb,
    ]
    if (adr.name) {
      lines.unshift(adr.name)
    }
    if (adr.state) {
      lines.push(adr.state)
    } else if (!adr.country) {
      lines.push('Victoria')
    }
    if (adr.country) {
      lines.push(adr.country)
    } else {
      lines.push('Australia')
    }
    return lines.join('\n')
  }
  getSlug() {
    return path.basename(path.dirname(this.thisPath))
  }
}

class Calendar {
  constructor({ thisPath, relPath, ...config }) {
    calendarLog(`New: ${thisPath}`)
    this.thisPath = thisPath
    this.relPath = relPath
    this.domain = config.domain
    this.url = `${httpScheme}://${config.domain}/events/${relPath}`

    const icalConfig = _.omitBy(config, (value, key) => xKeyFilter(key))
    const xKeyConfig = _.chain(config)
          .pickBy((value, key) => xKeyFilter(key))
          .mapKeys((value, key) => key.toUpperCase())
          .value()

    this.ical = ical({
      ttl: 60 * 60 * 24,
      ...icalConfig
    })
    for (const args of Object.entries(xKeyConfig)) {
      this.ical.x(...args)
    }

    this.addEvent = this.addEvent.bind(this)
    this.write = this.write.bind(this)
  }
  async addEvent(event) {
    const [
      fm,
      slug,
      organizer,
      location,
      description,
      htmlDescription,
    ] = await Promise.all([
      event.getFrontmatter(),
      event.getSlug(),
      event.getOrganizer(),
      event.getLocation(),
      event.getContentText(),
      event.getContentHtml(),
    ]);

    const extraConfig = {}

    const {
      timezone = 'Australia/Melbourne',
      eventGroups = [],
    } = fm

    for (const key of ['start', 'end']) {
      const val = fm[key]
      if (_.isString(val)) {
        extraConfig[key] = moment.tz(val, timezone)
      } else {
        const str = val.toJSON()
        extraConfig[key] = moment.tz(str.substr(0, str.length - 1), timezone)
      }
    }

    if (organizer) {
      extraConfig.organizer = organizer;
    }
    if (location) {
      extraConfig.location = location;
    }

    calendarLog(`Adding: ${slug}; ${extraConfig.start.toJSON()} - ${extraConfig.end.toJSON()}`)
    this.ical.createEvent({
      description,
      htmlDescription,
      timezone,

      uid: slug,
      summary: fm.title,
      categories: eventGroups.map(name => ({ name })),
      url: `${httpScheme}://${this.domain}/events/${slug}/`,

      ...extraConfig,
    })
  }

  async write() {
    await fse.ensureDir(path.dirname(path.join(this.thisPath)))
    return fse.outputFile(this.thisPath, this.ical.toString())
  }
}


function loadEvents(thisPath) {
  return new Promise(resolve => {
    const events = []
    const emitter = walk.walk(thisPath)
    emitter.on('file', filePath => {
      const relPath = path.relative(thisPath, filePath)
      if (relPath.indexOf(path.sep) === -1) {
        return
      }
      events.push(new Event({ thisPath: filePath }))
    })
    emitter.on('end', () => resolve(events))
  })
}


async function main() {
  const calendars = {
    index: new Calendar({
      thisPath: path.join(eventsPath, 'groups/index/index.ics'),
      relPath: 'groups/index/index.ics',
      domain: process.env.SITE_DOMAIN || 'localhost:3000',
      ...require('./config.json'),
    })
  }
  const events = await loadEvents(eventsPath)
  for (const ev of events) {
    const frontmatter = await ev.getFrontmatter()
    if (_.isEmpty(frontmatter)) {
      continue
    }
    const { eventGroups = [] } = frontmatter
    for (const eg of [...eventGroups, 'index']) {
      if (!calendars[eg]) {
        calendars[eg] = new Calendar({
          thisPath: path.join(eventsPath, `groups/${eg}/index.ics`),
          relPath: `groups/${eg}/index.ics`,
          ...require('./config.json')
        })
      }
      await calendars[eg].addEvent(ev)
    }
  }
  for (const cal of Object.values(calendars)) {
    await cal.write()
  }
}

module.exports = {
  Calendar,
  Event,
  main,
}

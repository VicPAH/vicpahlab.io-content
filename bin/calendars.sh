#!/bin/sh
THISDIR="$(cd "$(dirname "$0")"; pwd)"
cd "$THISDIR/lib"
yarn install
cd "$THISDIR/calendars"
yarn install
yarn start

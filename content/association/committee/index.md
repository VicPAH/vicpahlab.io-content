---
title: Committee members
layout: committee
members:
- name: Schism
  role: President
  image: schism.jpg
  prefix: He/Him
  email: president@vicpah.org.au
- name: Biscuit
  role: Vice-President
  prefix: He/Him
  image: biscuit.jpg
  email: vicepresident@vicpah.org.au
- name: Chalk
  role: Secretary
  image: chalk.jpg
  prefix: He/Him
  email: secretary@vicpah.org.au
- name: Wolfio
  role: Treasurer
  prefix: He/Him
  email: treasurer@vicpah.org.au
- name: Neliak
  role: General member
  image: neliak.jpg
  prefix: He/Him
  email: neliak@vicpah.org.au
- name: Plug
  role: General member
  image: plug.jpg
  prefix: He/Him
  email: plug@vicpah.org.au
---
# VicPAH committee members
Committee members meet at least once monthly to discuss relevant business relating to the
running of VicPAH, as well as ensuring that all VicPAH-run events run smoothly.

# Inclusion Statement

We believe that pet play is for all people: of all sexualities and none, all genders and none, all ages, all races and ethnicities, and all abilities.

We recognise, however, that segregated spaces can facilitate inclusion of those who may not feel comfortable or safe in mixed environments. The creation of any such spaces must not be at the cost of our overarching support of diversity and inclusion, and must be supported by a clearly articulated support of the association’s primary purpose.

We also recognise the legal and ethical risks of those yet to reach the age of majority. Inclusion in spaces created, or managed by the Association must be limited to people over the age of 18, except by an explicit decision of the Association supported by a robust risk management plan.

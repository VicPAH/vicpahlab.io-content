---
title: 2021 VicPAH Annual General Meeting
---
# 2021 VicPAH Annual General Meeting

## Notice of Annual General Meeting
Notice is hereby given under Rule 33(1) of the Rules of the Victorian Pups and Handlers Inc (“the  Association”) that the Annual General Meeting of the Association is be held at:

**<center>2 pm on Sunday 20 June 2021</center>**<br/>
**<center>Factory 7/2D Indwe St, West Footscray</center>**<br/>
**<center>-or-</center>**<br/>
**<center>online if required by state restrictions</center>**

The Agenda for the Annual General Meeting is:

### Ordinary Business

1. Receive from the Committee reports on the transactions of the Association during the last preceding financial year.
2. Receive and consider the financial statement submitted by the Association in accordance with section 97 of the *Associations Incorporation Reform Act 2012* (Vic).
3. Set the fee for a one-year membership of the Association at:
   1. $35 for standard membership price
   2. $20 for concession membership price
4. Special Resolution 1: Set the number of Ordinary Members of the Committee to a maximum of four (4):
5. Elect officers of the Association and the ordinary members of the Committee, being:
   1. one President
   2. one Vice President
   3. one Treasurer
   4. one Secretary
   5. up to four Ordinary Members
  
   *Nomination of candidates for election as officers of the Association or as ordinary members of the Committee must be made on the attached form and must be received by the Secretary [secretary@vicpah.org.au](mailto:secretary@vicpah.org.au) by 6pm on Friday 11 June 2021.*
  
   *Members who are unable to attend the Annual General Meeting in person may nominate a proxy to vote on their behalf by completing and signing the attached proxy form. The proxy must be a current member of the Association. The completed and signed Proxy form must reach the Secretary at [secretary@vicpah.org.au](mailto:secretary@vicpah.org.au) by 6pm on Friday 11 June 2021. Whilst attendance at the meeting can be by electronic means, all voting by members not present must be via proxy*

## Attachments
| Title                            | PDF                                                                                           | DOCX                                                                                          |
| -----                            | ---                                                                                           | ----                                                                                          |
| Notice of Annual General Meeting | [Download](</content/association/agm/2021/VicPAH June 2021 AGM Notice of Annual General Meeting.pdf>) | [Download](</content/association/agm/2021/VicPAH June 2021 AGM Notice of Annual General Meeting.docx>) |
| Vote by Proxy Form               | [Download](</content/association/agm/2021/VicPAH June 2021 AGM Vote by Proxy Form.pdf>)               | [Download](</content/association/agm/2021/VicPAH June 2021 AGM Vote by Proxy Form.docx>)              |
| Committee Nomination Form        | [Download](</content/association/agm/2021/VicPAH June 2021 AGM Committee Nomination Form.pdf>)        | [Download](</content/association/agm/2021/VicPAH June 2021 AGM Committee Nomination Form.docx>)       |

# Discord voice chat setup for Netflix Party

Voice chat with a movie in the background is tricky, especially when we have
a number of people all watching the same thing! To strike a balance between
having a relaxed community feel, and just a lot of background noise we'd like
to ask that:

- Only enable "voice activity" mode when you have a headset, or handsfree
  headset etc (otherwise, use push to talk). Movies have quite a range of
  volumes, and you might think that you've adjusted it correctly, and then it
  activates randomly during a loud scene
- Set your voice sensitivity manually, and push it just a bit higher than you
  think it needs to be
- When using push to talk, don't just leave your mic on!

## Voice chat settings (mobile)

You can find the voice chat settings by clicking the "gear" icon in the top
right corner of the voice chat<br/>
![Voice chat settings](/assets/netflix-party/discord-voice-settings-mob.png)

## Voice chat settings (desktop)

Voice chat settings on desktop are in your user settings.

Click the "gear" icon in the bottom left of your screen, near your user's
avatar picture<br/>
![Voice chat settings](/assets/netflix-party/discord-voice-settings-dt.png)

In the menu bar to the left, locate the "Voice & Video" option<br/>
![Voice chat settings](/assets/netflix-party/discord-voice-settings-dt2.png)

## Switching between push to talk, and voice activity (mobile)

In the voice chat settings window, the first drop down allows you to choose
between voice activated (default), and push to talk modes<br/>
![Voice chat input mode](/assets/netflix-party/discord-voice-input-mode.png)

A box will pop up, asking you what you'd like<br/>
![Voice chat input mode choices](/assets/netflix-party/discord-voice-choose.png)

## Switching between push to talk, and voice activity (desktop)

In the voice chat settings window, there's 2 options shown to select<br/>
![Voice chat input mode](/assets/netflix-party/discord-voice-input-mode-dt.png)

## Setting voice sensitivity

The graphics between mobile and desktop _look_ slightly different, but are
otherwise functionally identical.

Uncheck the "automatically determine input sensitivity", or "auto sensitivity"
option<br/>
![Voice chat automatic sensitivity](/assets/netflix-party/discord-voice-auto-sens-off.png)

A slider will replace the green bar underneath in the "sensitivity" section.
You can drag this left (more sensitive) or right (less sensitive)<br/>
![Voice chat automatic sensitivity](/assets/netflix-party/discord-voice-manual-sens.png)

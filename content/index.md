Membership payments are currently down. You can pay membership dues by emailing bark@vicpah.org.au to request a web invoice be emailed to you.

Victorian Pups and Handlers is an association to provide community-based support, promotion, and opportunity to experience pet play within the state of Victoria to a broad range of people of diverse abilities, genders, sexualities, and racial and ethnic backgrounds.

We try to accomplish this by:
- Promoting the use of pet play to aid in maintaining personal mental health (eg self esteem, social isolation)
- Promoting respect and tolerance for, and [social inclusion](/association/inclusion/) of people who identify as part of the pet play community so they can fully participate in all aspects of life without fear of discrimination
- Providing community spaces where members, and guests feel comfortable to meet, socialise, and explore pet play
- Supporting the pet play community with information to help make the most of their pet play experiences

We are group that encourages safe, sane, and respectful friendships and networks. To that end, we run several regular events 

- (in person) [Moshes](/events/group/mosh/): Events with pup hoods, play toys, and space to pup out. Runs every second month
- (in person) [Munches](/events/group/munch/): Friendly dinner get-togethers. Runs every other month

We are a not-for-profit organisation, and are governed by a strict set of [rules](/association/constitution/) to ensure that members are able to run the group to the greatest possible benefit of the community at large.

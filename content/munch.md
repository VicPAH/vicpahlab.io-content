---
title: Monthly Munch
layout: eventinfo
eventGroup: munch
---
Come have dinner at a restaurant bar, and get to know like-minded pups and handlers.

Tho Tho is a bar and restaurant that serves Vietnamese food. We're in the the
back room :)

We welcome all members of our diverse community to attend regardless of gender
or sexual expression.

## Cost
Free to attend, food extra

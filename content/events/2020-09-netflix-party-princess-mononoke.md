---
title: "Netflix Party - Princess Mononoke"
timezone: australia/melbourne
start: 2020-09-26T21:00:00
end: 2020-09-26T23:14:00
eventGroups: [vicpah, netflix]
movieId: 192569
synopsisText: "Ashitaka, a prince of the disappearing Emishi people, is cursed by a demonized boar god and must journey to the west to find a cure. Along the way, he encounters San, a young human woman fighting to protect the forest, and Lady Eboshi, who is trying to destroy it. Ashitaka must find a way to bring balance to this conflict."
partyLink: https://www.netflix.com/watch/28630857?npSessionId=d7e172c8d0bf5f88&npServerId=s92
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our final movie for the day will be Princess Mononoke

## Party link
[Click here to join](https://www.netflix.com/watch/28630857?npSessionId=d7e172c8d0bf5f88&npServerId=s92)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
Ashitaka, a prince of the disappearing Emishi people, is cursed by a demonized boar god and must journey to the west to find a cure. Along the way, he encounters San, a young human woman fighting to protect the forest, and Lady Eboshi, who is trying to destroy it. Ashitaka must find a way to bring balance to this conflict.

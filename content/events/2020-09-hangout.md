---
title: September Video Hangout
timezone: australia/melbourne
start: 2020-09-04T18:00:00
end: 2020-09-04T21:00:00
eventGroups: [vicpah, hangout]
link: https://meet.google.com/rxy-nbid-oot
---
VicPAH is having a Video Hangout on the Friday the 4th of September!

This time we'll be testing out a new online version of Cards Against Humanity, with a pack of PAH themed cards!

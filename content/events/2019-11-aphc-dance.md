---
title: Gear'd backdoor dance party
timezone: australia/adelaide
start: 2019-11-16T21:00:00
end: 2019-11-17T02:00:00
eventGroups: [aphc, interstate, major]
link: https://www.geardadelaide.com/events/geard-adelaide-backdoor-dance-party
adr:
  name: The Wakefield Hotel
  street_address: 76 Wakefield Street
  suburb: Adelaide
  state: South Australia
---
Gear’d Adelaide Backdoor Dance Party is back and bigger than ever before!

Come celebrate till the wee hours with the kinky folk and your new titleholders
for the 2019, Interstate and local DJ’s with play equipment and Play areas
provided.

This is an all inclusive event.

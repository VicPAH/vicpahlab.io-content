---
title: "Netflix Party - Labyrinth"
timezone: australia/melbourne
start: 2020-09-12T19:00:00
end: 2020-09-12T20:41:00
eventGroups: [vicpah, netflix]
movieId: 120386
synopsisText: "When teen Sarah is forced to babysit Toby, her baby stepbrother, she summons Jareth the Goblin King to take him away. When he is actually kidnapped, Sarah is given just thirteen hours to solve a labyrinth and rescue him."
partyLink: https://www.netflix.com/watch/680020?npSessionId=f7225f9c298f9ce4&npServerId=s33
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

We will only be watching 1 movie for tonight: Labyrinth

## Party link
[Click here to join](https://www.netflix.com/watch/680020?npSessionId=f7225f9c298f9ce4&npServerId=s33)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
When teen Sarah is forced to babysit Toby, her baby stepbrother, she summons Jareth the Goblin King to take him away. When he is actually kidnapped, Sarah is given just thirteen hours to solve a labyrinth and rescue him.

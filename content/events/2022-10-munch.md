---
title: September Munch
timezone: australia/melbourne
start: 2022-10-14T18:00:00
end: 2022-10-14T22:00:00
eventGroups: [vicpah, munch]
cost: $15 for members, $20.30 for non-members (pizza $5.30 for members, $7.10 for non-members)
adr:
  name: The Yorkshire Stingo Hotel
  street_address: 48 Hoddle St
  suburb: Abbotsford
  state: VIC
---
VicPAH is having a Munch on Friday the 14th of October, at the Yorkshire Stingo Hotel!
We'll be inside at 6PM.<br/>

Whether you're an experienced Puppy, Pet, or Handler, or simply curious, We have a welcoming and safe space for you to enjoy.
Munches are events where you can meet and chat with others in the community over dinner.
It’s a great way to dip your feet into the community if you’re not ready to attend a mosh yet.<br/>

We remind everyone that there are many in our community who are immuno-compromised. Please make sure your vaccinations are up to date, or have a valid exemption.<br/>


If you have any questions, don't hesitate to reach out to bark@vicpah.org.au or via one of our social media groups.<br/>


𝐌𝐞𝐧𝐮
https://www.yorkshirestingohotel.com.au/eat<br/>

If you have any questions, concerns, or if you're feeling nervous, please don't hesitate to reach out to [bark@vicpah.org.au](mailto:bark@vicpah.org.au) or via one of our social media groups.

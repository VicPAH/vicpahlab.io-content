---
title: "Netflix Party - Matilda"
timezone: australia/melbourne
start: 2020-09-26T17:00:00
end: 2020-09-26T18:38:00
eventGroups: [vicpah, netflix]
movieId: 109144
synopsisText: "An extraordinarily intelligent young girl from a cruel and uncaring family discovers she possesses telekinetic powers and is sent off to a school headed by a tyrannical principal."
partyLink: https://www.netflix.com/watch/70033005?npSessionId=dd079ee25530d3ad&npServerId=s47
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our first movie for the day will be Matilda

## Party link
[Click here to join](https://www.netflix.com/watch/70033005?npSessionId=dd079ee25530d3ad&npServerId=s47)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
An extraordinarily intelligent young girl from a cruel and uncaring family discovers she possesses telekinetic powers and is sent off to a school headed by a tyrannical principal.

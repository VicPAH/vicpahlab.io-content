---
title: April Mosh
timezone: australia/melbourne
start: 2022-04-24T13:00:00
end: 2022-04-24T17:00:00
eventGroups: [vicpah, mosh]
cost: $15 for members, $20.30 for non-members (pizza $5.30 for members, $7.10 for non-members)
dressCode: |-
  This is our private mosh venue, so leather, rubber, neoprene gear is acceptable! No nudity.
adr:
  name: The Club
  street_address: 7/2D Indwe St
  suburb: West Footscray
  state: VIC
tryBookingEid: 888664
---
VicPAH is having a mosh on the 24th of April.
We'll be having pizza!<br/>

Since this is held at a private venue, we can wear any gear we like (as long as there is no nudity.) Leather, rubber, neoprene, drag, anything you like!<br/>
Our meets are open to all diverse gender, and sexual identities.<br/>
whether you're an experienced Pup or Handler, or simply curious to see if it's something that may interest you, we have a welcoming and safe space for you to enjoy.<br/>

This event will be following necessary covid safe guidelines.<br/>
If you are experiencing financial hardship and are unable to afford a ticket, or if you have any questions, concerns, or you're just feeling nervous,
please don't hesitate to reach out to [bark@vicpah.org.au](mailto:bark@vicpah.org.au) or via one of our social media groups.

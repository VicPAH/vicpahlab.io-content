---
title: VicPAH 6th Birthday Party
timezone: australia/melbourne
start: 2022-05-06T18:00:00
end: 2022-05-06T22:00:00
eventGroups: [vicpah]
cost: |-
  There will be no ticketing/fees to enter the event, it will be open to the public. Raffle tickets are $5 or 3 for $10.
  
  Check out [dtshotel.com.au](https://www.dtshotel.com.au/) to see the prices for pizza.
dressCode: |-
  We can wear any gear we like as long as there is no nudity, including explicit crotch bulges under clothing. Otherwise wear leather, rubber, neoprene, drag, anything you like!
  
  The venue will still be open to the general public, so make sure you are comfortable being seem by muggles in your outfit.
adr:
  name: DT's Hotel
  street_address: 164 Church St
  suburb: Richmond
  state: VIC
---
VicPAH is celebrating our 7th Birthday at DT's Hotel!<br/>
There'll be a small ball pit puppy area in the back room, pizza, a rope demo, cake, a raffle, and more!

Our meets are open to all diverse gender, and sexual identities. Whether you're an experienced Pup or Handler, or simply curious to see if it's something that may interest you, we have a welcoming and safe space for you to enjoy.

To enter the mosh, it is mandatory to be either, double dose vaccinated, or hold a valid exemption.
Services Vic check-in is mandatory.

If you have any questions, concerns, or if you're feeling nervous, please don't hesitate to reach out to [bark@vicpah.org.au](mailto:bark@vicpah.org.au) or via one of our social media groups.

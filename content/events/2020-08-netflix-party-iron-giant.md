---
title: "Netflix Party - The Iron Giant"
timezone: australia/melbourne
start: 2020-08-22T21:35:00
end: 2020-08-22T23:01:00
eventGroups: [vicpah, netflix]
movieId: undefined
synopsisText: "Amid the backdrop of the Cold War, a 9-year-old boy befriends a colossal yet gentle robot that a paranoid government agent is intent on destroying."
partyLink: https://www.netflix.com/watch/70015683?npSessionId=e932f76ca52ea19f&npServerId=s74
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our second movie for the day will be The Iron Giant

## Party link
[Click here to join](https://www.netflix.com/watch/70015683?npSessionId=e932f76ca52ea19f&npServerId=s74)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
Amid the backdrop of the Cold War, a 9-year-old boy befriends a colossal yet gentle robot that a paranoid government agent is intent on destroying.

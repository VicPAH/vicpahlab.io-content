---
title: Netflix Party - Howl's Moving Castle
timezone: australia/melbourne
start: 2020-08-01T21:00:00
end: 2020-08-01T22:59:00
eventGroups: [vicpah, netflix]
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our final movie for the day will be Howl's Moving Castle.

## Party link
[Click here to join](https://www.netflix.com/watch/70028883?npSessionId=1f363a35034a6a7f&npServerId=s26)

Remember to click the Netflix Party extension icon to join the party!

<!-- The party link will be added here shortly before the movie is about to begin. -->

## Synopsis
Teenager Sophie works in her late father's hat shop in a humdrum town, but things get interesting when she's transformed into an elderly woman.

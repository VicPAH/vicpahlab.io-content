---
title: Midsumma carnival
timezone: australia/melbourne
start: 2020-01-19T11:00:00
end: 2020-01-19T22:00:00
eventGroups: [vicpah, victoria, midsumma, major, free]
link: https://www.midsumma.org.au/info/midsumma-carnival-info/
facebook: https://www.facebook.com/midsumma
sendTelegram: true
cost: Free
adr:
  name: Alexandra Gardens
  street_address: 5 Boathouse Dr
  suburb: Melbourne
  state: VIC
---
Midsumma Carnival highlights the opening weekend of the three-week Festival each year. Midsumma Carnival is an iconic outdoor celebration that has become one of the biggest highlights in the LGBTIQA+ annual calendar. The event provides a fitting opening to the Festival each year. In itself, Carnival is a huge single-day event running from 11am until 10pm in Alexandra Gardens in Melbourne’s CBD, with a massive set up and overall coordination required for delivery each year. 

Midsumma Carnival attracts a broad attendance across age ranges and demographics, truly celebrating a day of inclusion and diversity in all its forms.

This popular annual event is free to the public and attracts around 118,000 people. 

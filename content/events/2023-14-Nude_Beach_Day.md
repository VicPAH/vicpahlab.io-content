---
title: Sunnyside Pups Nude Beach
timezone: australia/melbourne
start: 2023-01-14T13:00:00
end: 2023-01-14T17:00:00
eventGroups: [vicpah]
cost: Free!
dressCode: |-
  Nude! Pet/Pup/Kitty etc gear is also totally OK.
adr:
  name: Sunnyside Nude Beach
  street_address: Sunnyside Nude Beach
  suburb: Mt Eliza
  state: VIC
---
Awrooo!

The sun is out again! and our tails are wagging! VicPAH is headed back to the beach! Sunnyside Nude Beach!

VicPAH is reigniting the nude beach event for the 14th of January from 1pm to 5pm! This gathering is not ticketed!

We currently have three designated drivers, Biscuit in the North suburbs, picking up from Rosana station! Neliak in the east picking up from Oakleigh station! Plug in Footscray picking up at Footscray Station. We can take 3 people each! Let us know if you want to come with us as we only have a limited number of seats. 

We also are looking for any more designated drivers from anywhere around Melbourne! If you would like to volunteer or grab a place in a car, please message either Biscuit or Neliak on Discord. It would be greatly appreciated!

Going via public transport? No problems, you will want to take the Frankston train line. You can get the bus from Frankston using the 781 784 and 785 to get near Sunnyside. 

What to bring! Yourself of course, a towel, sunscreen, food and drinks and other necessities! We may also recommend some shade like an umbrella! Neliak and I may also go for a food run during the event so feel free to pitch in! 

This is a non sexual event, remember to be safe, sane and consensual and always ask before touching others. We strive to have a fun nudist experience where everyone can feel comfortable in their skin no matter their gender, ethnicity or size.
If you have any questions, concerns, or you're just feeling nervous,
please don't hesitate to reach out to [bark@vicpah.org.au](mailto:bark@vicpah.org.au) or via one of our social media groups.<br/>


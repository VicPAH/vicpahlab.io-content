---
title: "Netflix Party - Invader ZIM: Enter the Florpus"
timezone: australia/melbourne
start: 2020-10-31T20:45:00
end: 2020-10-31T21:57:00
eventGroups: [vicpah, netflix]
movieId: 524282
synopsisText: "ZIM discovers his almighty leaders never had any intention of coming to Earth and he loses confidence in himself for the first time in his life, which is the big break his human nemesis, Dib has been waiting for."
partyLink: https://www.tele.pe/netflix/dd0b062860c627f0?s=s117
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

Our final movie for the day will be Invader ZIM: Enter the Florpus

## Party link
[Click here to join](https://www.tele.pe/netflix/dd0b062860c627f0?s=s117)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
ZIM discovers his almighty leaders never had any intention of coming to Earth and he loses confidence in himself for the first time in his life, which is the big break his human nemesis, Dib has been waiting for.

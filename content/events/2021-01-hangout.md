---
title: January Video Hangout
timezone: australia/melbourne
start: 2021-01-30T18:30:00
end: 2021-01-30T21:30:00
eventGroups: [vicpah, hangout]
---
VicPAH is having a Video Hangout on the Saturday the 30th of January! The link will be provided on the day of the meeting.

We're playing Pups Against Humanity again!

Our meets are open to all diverse gender, and sexual identities.

Whether you're an experienced Pup or Handler, or simply curious to see if it's something that may interest you, we have a welcoming and safe space for you to enjoy.

Come wearing whatever Pup/Pet/Handler gear you like!

Standard VicPAH rules apply, no nudity, graphic imagery, or hate speech. Please respect everyone and yourself at all times.

You don't need a Google Account to join the meeting. You're also welcome to join without video/voice, and just use the text chat.

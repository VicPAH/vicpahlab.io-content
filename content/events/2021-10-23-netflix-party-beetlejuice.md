---
title: "Netflix Party - Beetlejuice"
timezone: australia/melbourne
start: 2021-10-23T19:00:00
end: 2021-10-23T20:32:00
eventGroups: [vicpah, netflix]
movieId: 39601
synopsisText: "Thanks to an untimely demise via drowning, a young couple end up as poltergeists in their New England farmhouse, where they fail to meet the challenge of scaring away the insufferable new owners, who want to make drastic changes. In desperation, the undead newlyweds turn to an expert frightmeister, but he's got a diabolical agenda of his own."
partyLink: https://redirect.teleparty.com/join/60f22d5efbc4d638
---
VicPAH [Netflix party](https://www.netflixparty.com/) events allow us to share
movies from our own kennels and couches. Join the chat, or just watch along
with the rest of the PAH; it's up to you! All our movies will be synced up so
that they play and pause at the same time so we see the same thing.

We will only be watching 1 movie for tonight: Beetlejuice

## Party link
[Click here to join](https://redirect.teleparty.com/join/60f22d5efbc4d638)

Remember to click the Netflix Party extension icon to join the party!

## Synopsis
Thanks to an untimely demise via drowning, a young couple end up as poltergeists in their New England farmhouse, where they fail to meet the challenge of scaring away the insufferable new owners, who want to make drastic changes. In desperation, the undead newlyweds turn to an expert frightmeister, but he's got a diabolical agenda of his own.

---
title: October Video Hangout 2 and Among Us
timezone: australia/melbourne
start: 2020-10-23T18:30:00
end: 2020-10-23T21:00:00
eventGroups: [vicpah, hangout]
link: https://meet.google.com/ssb-hiyr-dzq
---
VicPAH is having a Video Hangout on the Friday the 23rd of October!

This time we'll be playing Among Us!

## Among Us
Play with 4-10 player online or via local WiFi as you attempt to prepare your spaceship for departure, but beware as one or more random players among the Crew are Impostors bent on killing everyone!

Originally created as a party game, we recommend playing with friends at a LAN party or online using voice chat. Enjoy cross-platform play between:

- [iPhone/iPad](https://apps.apple.com/au/app/among-us/id1351168404) (free)
- [Android](https://play.google.com/store/apps/details?id=com.innersloth.spacemafia) (free)
- [Windows](https://store.steampowered.com/app/945360/Among_Us/) ($7.50)

# VicPAH howl Telegram group rules
This channel is 18+ only due to the nature of discussions that can and do occur,
however all media content is to be SFW. If you’re looking for a more sexual
Telegram channel, we recommend [OZ Dirty Dogs](https://t.me/joinchat/CdyJNgrnA9YXgrJKrCJ96w).
It’s well moderated, and abides by the same inclusive values that VicPAH prides
itself on. We also have kink, and general NSFW channels on our [Discord server](https://discord.gg/AGUvEmb).


## Rules
1. No hate speech, Bullying, or degrading comments about things such as race, religion, culture, sexual orientation, gender or identity. No shit-posting, kink-shaming, or general discriminatory behaviour.<br/>
   Comments which are derogatory or intentionally provocative will be deleted. Repeat offenders will be removed.
2. SFW Content only: No nudity, violent/shocking imagery, or pornography.<br/>
   Puppy tails, tassles, and content that has a reasonable educational focus (sex/fetish education) is allowed though.
3. Use common sense. Don’t ask moderators to settle private disputes; it will not happen.
4. Do not flood images. If you have a large number of pictures to share consider utilising the albums function. (Pictures must fit the above rule of SFW content.)<br/>
   Maximum of ten (10) images within a fifteen (15) minute period, further content may be queued for later sending
5. Avoid drama, including provocation and hostility. Private or otherwise, it doesn’t belong in a public chat.<br/>
   Do not ask mods to handle or moderate private issues between users.
6. No sexual solicitation: This isn’t a personals page. This may be an 18+ group, however remember that we have pets and handlers that don’t always appreciate overtly sexual content.
7. Do not promote external groups without permission from the admins. Promotion doesn’t just include direct linking: If you are unsure, ask first.
8. Do not attempt to gather sensitive information, or compromise the security or normal operation of our users’ devices, accounts, or other personal details.<br/>
   This may include seeking phone numbers, addresses, real names, passwords, sending viruses, pirated content, content designed to crash apps/devices, etc

## DNP List
* Off-topic media: content that is not related to the group theme. Politics, news, memes, GIFs, etc are considered off-topic and should be kept to a minimum.
* NSFW material.
* Repeated self-promotion unrelated to pup play (eg game streaming, YouTube channels, etc). Letting people know where you do things once is fine.
  * For game streaming, we have a [Discord channel](https://discord.gg/XcXS4Ut) specifically for these announcements

Please PM any of the admins if you have questions. They can be identified by a star next to their name in the member list
